# [ NexusStar.name ](http://nexusstar.name) website blog

---

## Installing and usage

_I am sure that there is better way but before finding it will use this_

* Clone repository

* Install dependency

    bundle install

* Serving local dev build using `_config_dev.yml` different config file

    bundle exec jekyll serve --config _confi_dev.yml
  or
    jekyll serve --watch --incremental --config _config_dev.yml

* Edit files
copy files to override from *minimal mistakes gem* using `bundle show minimal-mistakes-jekyll`

* Use `_site` folder as separate repository for automating deployment

* Build with default `_config.yml` file before pushing

    bundle exec jekyll build

---

## Credits

### Theme Creator

[Minimal Mistakes Jekyll Theme](https://mmistakes.github.io/minimal-mistakes/)

**Michael Rose**

- <https://mademistakes.com>
- <https://twitter.com/mmistakes>
- <https://github.com/mmistakes>
