---
layout: archive
title: "Hackpad"
permalink: /hackpad/
author_profile: true
---
Some of this notes was created at: [ hackpad.com ](https://hackpad.com) as it's superb tool
I think my notes are easily maintainable this way.

{% include base_path %}

{% for post in site.hackpads %}
  {% include archive-single.html %}
{% endfor %}
