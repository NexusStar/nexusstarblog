---
title: "About me"
layout: single
excerpt: "What drives me and keeps me awake late at night"
sitemap: true
author_profile: true
permalink: /about/
---

Over the years, I’ve had the pleasure to work with really great people and companies on some really amazing projects.

Not to get confused if you care NexusStar was not my real name.

My name is Petar Nikov I am father, designer, developer, coder.
Mostly in that order but recently dabbling in programming a lot.

## What I do that passes for work hours

Most of my daily hours I spend designing stuff and codding stuff.

Designing mostly traditional print materials, and when needed I do
and have done also web-design.

I consider myself pretty good front-end developer and Drupal enthusiast.
If that you can call someone with more than 15 Drupal sites under my belt
and 4 year experience.

While I love my daily job I really love programming and learning it.

## What I do for fun

*I love to learn new things*, listening to audio books and reading.

[ Tweeking ](https://github.com/nexusstar/dotfiles) my working environment.

And one day hope to have time and money to windsurf all day long without rushing and care.
