---
title: "Spacemacs Cheat Sheet"
excerpt: "vim-mode cheatsheet for spacemacs"
created: 2016-12-28
modified: 2017-01-15
tags: [spacemacs, begginer]
---

## Get help, quit and configure

## Editor

### Basic commands

| Key Binding   | Description                                                 |
|---------------|-------------------------------------------------------------|
| **SPC q q**   | Quit                                                        |
| **SPC z x**   | Increase/decrease font size                                 |
| **SPC f e d** | Edit `.spacemacs` config files                              |
| **SPC q r** | Restart Spacemacs                              |
| **f d**       | evil-escape from `insert` and everything else               |
| **SPC a u**   | Undo tree visualizer `q` to quit it. `C-n``C-p` to navigate |
| **SPC t w**   | Show whitespace                                             |
| **SPC :**     | type `tabify` spaces to tabs                                |
| **SPC :**     | type `untabify` convert tabs to spaces                      |

#### Getting help

Help opens up with `SPC h`

| Key Binding | Description               |
|-------------|---------------------------|
| **SPC h m** | describe the current mode |
|             |                           |



#### Registers

| Key Binding | Description                        |
|-------------|------------------------------------|
| **SPC r e** | show evil yank and named registers |
| **SPC r m** | show marks register                |
| **SPC r r** | show helm register                 |
| **SPC r y** | show kill ring                     |

#### Windows

| Key Bindings | Description                |
| ------------ | -----------                |
| **SPC w S**  | Split horizontal and focus |
| **SPC w v**  | Split window               |
| **SPC w V**  | Split window and focus     |
| **SPC w c**  | Close window               |

#### Ido vertical mode

| Key Bindings | Description             |
| ------------ | -----------             |
| **C-n**      | Next result             |
| **C-p**      | Previous result         |
| **C-o**      | Open file in new window |
| **C-RET**    | Open a dired buffer     |

#### NeoTree

| Key Bindings | Description                               |
| ------------ | -----------                               |
| **SPC f t**  | _Open/Close_ NeoTree in current directory |
| **SPC p t**  | _Open/Close_ NeoTree in project root      |
| ** hjkl **   | navigate                                  |
| ** H **      | Up                                        |


## Working File manipulation

### Previous next commands `[]`
[ -> previous ] -> next

| Key Bindings | Description    |
|--------------|----------------|
| **]e**       | Move line down |
| **[e**       | Move line up   |

### Jumps `j`

| Key Bindings | Description           |
|--------------|-----------------------|
| **SPC j l**  | Jump to line with avy |
| **SPC j b**  | Jump back             |
| **SPC j w**  | Avy jump word         |


### Searching and replacing

| Key Bindings  | Description                |
|---------------|----------------------------|
| **\\**        | Search symbol below cursor |
| **SPC s a a** | Ag this file               |
| **/**         | Search in file             |
| **SPC /**     | Helm Ag search in project  |

Substitute works as in Vim

Replaces all foo with bar

```bash

:s%/foo/bar/g 

```

Replace but ask first

```bash
:s%/foo/bar/gc
```
#### Replace all matches in project

* Search with `SPC /`
* Enter in edit mode `C-c C-e`
* Go to the occurrences and enter in iedit state with `SPC s e`
* Edit and then leave the _iedit sate_
* Exit edit mode `C-c C-c`



### Project

| Key Bindings  | Description               |
|---------------|---------------------------|
| **SPC p b**   | Switch  to buffer         |
| **SPC p p**   | Switch project            |
| **SPC p f**   | Find in project           |
| **SPC p h**   | File helm                 |
| **SPC s a f** | Ag file name in directory |
