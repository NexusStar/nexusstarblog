---
title: "Install AUR package-query and yaourt"
excerpt: "Manually install AUR & yourt packages"
modified: 2016-02-25
tags: [beginner, ArchLinux]
---


You can use the  `~/temp/AUR/` to manually install AUR packages:

    $ mkdir -p ~/temp/AUR/ && cd ~/temp/AUR/

First, we install package-query…

    $ wget https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz  # download source tarball
    $ tar xfz package-query.tar.gz  # unpack tarball
    $ cd package-query  &&  makepkg  # cd and create package from source
    $ sudo pacman -U package-query*.pkg.tar.xz  # install package - need root privileges

…and then Yaourt in the same way:

    $ wget https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz
    $ tar xzf yaourt.tar.gz
    $ cd yaourt  &&  makepkg
    $ sudo pacman -U yaourt*.pkg.tar.xz

_Note that you run Yaourt as a normal user, without sudo.  sudo will be called (and you will be asked for your user password) after the package has been compiled, and before it is installed into your live system._
