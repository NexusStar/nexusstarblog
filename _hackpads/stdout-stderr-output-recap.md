---
title: "Shell script's' idiom 2>&1 recap"
excerpt: "stdour stderr descriptors"
created: 2018-03-21
modified: 2018-03-21
tags: [linux, beginner]
---

There is excelent article from [Brian Storti](https://www.brianstorti.com/understanding-shell-script-idiom-redirect/) back from 2015 about shell script's idiom 2>&1

_So to note important points_

### File descriptors
File descriptors are positive integer number that uniquely represents an opened file in operating system.

### Recap of I/O redirection

  * There are two places programs send output to: Standard output (stdout) and Standard Error (stderr);

  * You can redirect these outputs to a different place (like a file);
  * File descriptors are used to identify `stdout (1)` and `stderr (2)`;
  * `command > output `is just a shortcut for `command 1> output;`
  * You can use &[FILE_DESCRIPTOR] to reference a file descriptor value;
  * Using `2>&1` will redirect stderr to whatever value is set to stdout (and `1>&2` will do the opposite).
