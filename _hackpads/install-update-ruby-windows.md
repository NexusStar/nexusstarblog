---
title: "Install / Update Ruby on windows"
excerpt: "update existing install and add DevKit"
modified: 2016-12-27
tags: [begginer, ruby, windows]
---

1. Install using downloads from [RubyInstaller](http://rubyinstaller.org/downloads/)
1. Check if correct version is installed with `ruby --version`
 - if not correct version reported check PATH 
   * Go to Control Panel -> System -> Advanced system settings -> Environment Variables
   * Check what is added in `path` variable
 
1. Download DevKit / unzip and configure
 - unzip package
 - go to unziped dir, in Git Bash preferably, and run:

```bash
    ruby dk.rb init
    ruby dk.rb install
    gem install rdiscount --platform=ruby
```
