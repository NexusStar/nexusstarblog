---
title: "Work with numbers in Vim"
excerpt: "insert numbers and even calc"
modified: 2016-11-05
tags: [vim, beginner]
---

* The expression register `=`
* `<C-a>` `<C-x>`in normal mode

## Expression register `=`

The expression register can evaluate a piece of Vim script code
Access it with `<C-r>=` in insert mode.
It can evaluate arithmetic expressions e.g:

```css
body { font-size: 16px; }
h1 { font-size: 1.25rem; //20px}
```
The `rem` value could be calculated directly in insert mode
with following `<C-r>=16.0/20.0<CR>` combination.

When on the right position type CTRL + r followed with = and
after that the arithmetic expression followed with `_ENTER_` the result
will be inserted automatically.

## Normal mode direct addition and subtraction  with counters

Using `<C-a>` will jump to first found number on line.
But if added some whole number it will add it to the number directly

```css
.pannel{padding 10px;} // 29<C-a> will change the line to
.pannel{padding 39px;}
```

Alternatively `<C-x>` will subtract from number so

```css
p { margin-top: 0px; } // 20<C-x> will change it to
p { margin-top: -20px;}
```

