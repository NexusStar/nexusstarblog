---
title: "Replace ^M windows line endings in vim"
excerpt: "replace annoying ^M artefacts that shows in vim file"
modified: 2016-08-03
tags: [vim, beginner]
---

When files comes from windows sometimes there are `^M` at end of all lines
which marks line ending.

So to remove them from file type:

```bash
    :%s/<Ctrl-V><Ctrl-M>/\r/g
```

Where <Ctrl-V><Ctrl-M> means type Ctrl+V then Ctrl+M.

## Explanation

```bash
    :%s
```

substitute, % = all lines

<Ctrl-V><Ctrl-M>

`^M` characters (the Ctrl-V is a Vim way of writing the Ctrl ^ character and Ctrl-M writes the M after the regular expression, resulting to ^M special character)

Replace:
```bash
    /\r/
```
with new line (\r)

```bash
  g
```

And do it globally (not just the first occurrence on the line).
