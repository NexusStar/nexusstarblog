---
layout: single
title: "Software composition notes"
excerpt: "lambda calculus"
modified: 2017-04-28
tags: [programming, beginner]
---

> In mathematics and computer science *curring* is the technique of translating the evaluation of a *function* that takes multiple *arguments* into evaluating a sequence of functions, each with a single argument.
Then n-ary function `(x,y)=> x+y;` can be expressed as a unary function like `x => y => x +y` this transformation from n-ary function to unary function is known as curryg.

