---
title: "Using Iedit mode in Spacemacs"
excerpt: "minor mode to edit all occurrence of some text in a buffer"
created: 2017-01-18
modified: 2017-01-18
tags: [spacemacs, begginer]
---
Spacemacs defines two states 

* _iedit_ state
* _iedit-insert_ state

Iedit can be started from search with `SPC s e`

| Key Binding   | Description                                                                                          |
|---------------+------------------------------------------------------------------------------------------------------|
| **RET**       | call default action on current candidate                                                             |
| **M-RET**     | the same as ~RET~ but doesn't close completion minibuffer                                            |
| **C-M-j**     | use current input immediately (this can be used to create a new file in Find File)                   |
| **tab**       | complete partially                                                                                   |
| **M-o**       | show the list of valid actions on current candidate (then press any of described keys to execute it) |
| **C-M-o**     | the same as ~M-o~ but doesn't close completion minibuffer                                            |
| **C-'**       | use avy to quickly select completion on current page (sometimes faster than using arrows)            |
| **<ESC>**     | close minibuffer                                                                                     |

** Transient state
Press ~M-SPC~ anytime in Ivy to get into the transient state.

| Key Binding   | Description                                             |
|---------------+---------------------------------------------------------|
| **j**         | select next candidate                                   |
| **k**         | select previous candidate                               |
| **d**         | call default action on candidate                        |
| **g**         | the same as ~d~ but doesn't close completion minibuffer |
| **o**         | leave transient state                                   |

Evil commands to go in insert mode will switch to _iedit-insert_ state
All commands have default Vim behavior when used outside of an _occurrence_

Region can be narrowed with visual selection.

Exited with `C-g` executed on matches or double `ESC`

In iedit mode

| Key Binding | Description                              |
|-------------|------------------------------------------|
| **TAB**     | Jump to next occurrence                  |
| **0**       | go to beginning of current occurrence    |
| **$**       | go to the end of  the current occurrence |
| **#**       | prefix all occurrences with number       |
| **C-U**     | down-case the occurrence                 |
| **U**       | up case the occurrences                  |

## Using iedit to replace all occurrence in project

* Search with `SPC /`
* Enter in edit mode `C-c C-e`
* Go to the occurrence and enter in _iedit_ state with `SPC s e`
* Edit and then leave the _iedit_ state
* Exit edit mode `C-c C-c`
