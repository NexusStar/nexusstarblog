---
title: "SVN changelist command"
excerpt: "your best friend for issue handling"
modified: 2016-10-26
tags: [svn, beginner]
---

I'm not a big fan of SVN but it was inescapable evil in some projects.

One of the best command from SVN is `changelist`.

Changelists are the best way to separate part of the project and
working on separate issues.

Adding file to changelist is stright forward:

```bash 
svn changelist 'issue01' file1 file2

Path file1 is now  part of changelist 'issue01'
Path file2 si now part of chagelist 'issue01'
```

When commiting changes we can commit only the changes of particular 
changelist:

```bash
svn ci --changelist 'issue01' -m "Issue 01 fixes"
```

This is extremely powerful as this allow us to work on multiple different, distinct part of 
our project.

`--changelist` options can be used on almost every svn command

```bash
svn st --changelist
svn diff --changelist
```
Even `changelist` command support `--changelist` option this allows us to rename and remove
a changelist.

```bash
svn changelist --remove --changelsit issue01 --depth infinity .
Path file1 is no longer member of changelist issue01
Path file2 is no longer member of changelist issue01
```

  `svn commit` have `--keep-changelists` option which preserves file assignments to changelist after commit



___CHANGELIST SUPPORSTS ONLY FILES NOT DIRECTORIES___

The shouting note is needed just because if you want to add _new_ files to 
changelist you can fall into the somewhat informative error:

`is not part of a working copy but it's children are`

