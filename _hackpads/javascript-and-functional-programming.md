---
layout: single
title: "JavaScript as functional programming language"
excerpt: "functional programming"
modified: 2017-04-28
tags: [programming, beginner, JavaScript]
---
JavaScript has the most important features needed for functional proragmming:

1. *First class functions*: The ability to use functions as data values: papss functions as arguments, return functions, add assign functions to variables and object properties. This property allows for higher order functions, which enable partial application, cyrrying, and composition

2. *Anonymous functions and concise lambda syntax* : `x => x * 2` is a valied function expression in JavaScript. Concise lambdas make it easier to work with higher-order functions.

3. *Closures*: A closure is the bundling of a function with its lexical environment
