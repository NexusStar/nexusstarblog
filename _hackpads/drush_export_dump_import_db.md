---
title: "Use Drush to Import/Export MySQL database file"
excerpt: "quickly export/import database using drush"
created: 2017-03-21
modified: 2017-03-21
tags: [drush, Drupal, beginner]
---

## Export Database to file

### Drupal 6 & 7

```bash
    drush cc
    drush sql-dump > ~/my-sql-dump-file-name.sql
```

### Drupal 8

```bash
    drush cr
    drush sql-dump > ~/my-sql-dump-file-name.sql
```

## Download Database using ssh and secure copy `scp`

```bash
    scp username@server-address.com:/file_path_remote/file_name.sql /local_path/
```

## Import Database

```bash
    drush sql-drop
    drush sql-cli < ~/my-sql-dump-file-name.sql
```

