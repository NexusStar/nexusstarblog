---
title: "Favor object composition over class inheritance"
excerpt: "what does it means"
modified: 2018-04-15
tags: [javascript, concepts]
---

["Design Patterns: Elements of Reusable Object-Oriented Software"](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)

Composition over inheritance (or composite reuse principle) in object-oriented programming (OOP) is the principle that classes should achieve polymorphic behavior and code reuse by their composition (by containing instances of other classes that implement the desired functionality) rather than inheritance from a base or parent class. _Wikipedia_

It means that code reuse should be achieved by assembling smaller unit of functionality into new objects instead from classes and creating object taxonomies.

In other words, use _can-do_, _has-a_, or _uses-a_ relationships instead of _is-a_ relationships.

Diffrences beween composition and class inheritance:
The purpose of composition: make whole out of parts.
The purpose of inheritance: It have semantic(meaning) with which it arange concepts from generalized to specialized, grupping releates concepts in subtrees. Capture mehanics, behavior(methods) of class and making it available for reuse and augumentationi.

