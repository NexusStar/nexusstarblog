---
title: "Responsive design"
excerpt: "responsive design core concepts"
created: 2018-04-06
modified: 2018-04-06
tags: [web, beginner]
---
[ Frontend Masters course  ](https://frontendmasters.com/courses/css-grids-flexbox/)I am currently watching from [Jen Kramer](http://www.jenkramer.org/index.php) bring to memory the article from Ethan Marcote definning what constitute responsive design:

Responsive design is not magic jumbo mumbo it is defined by three characteristics:

* Flexible grid based layouts
* Media queries(CSS3)
* Images that resize

It's always better to remeber and sometimes to revisit the basics:
[http://alistapart.com/article/responsive-web-design](http://alistapart.com/article/responsive-web-design)
