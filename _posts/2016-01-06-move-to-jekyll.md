---
layout: single
title: "Rebuild with Jekyll"
excerpt: "Rebuild my personal website with jekyll"
modified: 2016-07-25
tags: [jekyll, personal, blog]
comments: true
author_profile: true
---

## Why

My personal website went to *too* many changes.

I start with simple static html and JS and later went on hunt for ultimate CMS
pass through Joomla went to WordPress and hated it from the start (will try it again for sure) and finaly ended in Drupal camp for good.

So after I choose to use Drupal for website creation the next reasonable step for me was to made my personal website also with this wonderful framework.

I went to more than few iteration to my web site with it.

## But

There is always big _but_ with every choice.

Drupal is great but it is quite a overkill for personal web site.
It needs a lot of modules, configuration etc.

And in the last 4 years I tend to lean mostly on the front-end development.
Using a lot of command line tools learning VIM and now switch to Neovim.

All that bring me back to static web-site and Jekyll

* I wanted to have cool design
* I wanted it to be simple to maintain
* I wanted it to be easy and changeable
* I wanted it to use markdown so to write every post in VIM

So here is my new blog and personal page using Jekyll
there are more than few bumps on the road and rebuilding on the go
_but_ I am never satisfied.


