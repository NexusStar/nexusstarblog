---
layout: single
title: "Consistency beats commitment"
excerpt: "two in a row"
modified: 2016-08-03
tags: [intro, beginner, programming]
comments: true
author_profile: true
---
_"Consistency beats commitment every time"_

Today just finished "Building and Managing Your Career Plan" course on Pluralsight by Jason Alba.

There he points above quote many times and recommend hotly [ "Hello world Podcast" ](https://wildermuth.com/hwpod) by Shawn Wildermuth where 

I listen there to [ Scott Hanselman ](http://www.hanselman.com/) interview he also quote above word by word.

What's chance of that one quote two time in just a day.

The lesson learned is that it's better to be consistent not just committed:

* Decide where you want to end with your life path

* Make a map how to go there
  - What are the step needed to arrive there
  - What you need to learn to achieve what you want
  - Don't be stickler and know that everything changes
  - _but be consistent about it_
