---
layout: single
title: "Shell suspend, kill and bring back on jobs"
excerpt: "using shell with job control"
modified: 2016-11-09
date: 2016-11-09
tags: [linux ,beginner, terminal]
comments: true
author_profile: true
---
* This should be true of any shell with job control

Hitting `Ctrl` + `z` will suspend currently running process and send it to
background, fastest way to bring it back on is running `fg`.

Explanations:

By default, a _process running in the terminal is a process running in the foreground_.
A process _can also run in background_.


### Interactive

* `Ctrl` + `z` will suspend the currently foregrounded program
* `bg` will background the most recently suspended program
  - To run a process in background, insert `&` after the process name

  ```bash
    firefox &
  ```
  _note_ the process could otputs to stderr or stdout so it could cluter terminal in that case
  redirect it's output to file or do something like this:

  ```bash
    bundle exec jekyll serve --incremental & > /dev/null
  ```

* `fg` will foreground the most recently suspended program

* `jobs` will list backgrounded process with their numbers
  - To kill a process use its job_spec `kill -9 %[job_spec]`
  - To start (unpause) suspended background process `bg %[job_spec]` or just `bg` for last suspended one.
  - Move job to foreground  `fg %[job_spec]`

### Non-interactive

In a diferent shell instance or user we need to know the PID

* get PID with `ps aux | grep ...` / `pgrep ...`
* act on process:
    
  ```bash
    kill -STOP $PID #suspend the process
    kill -CONT $PID #continue (resume) process
  ```
  or using program name

  ```bash
    killall -STOP program_name
    pkill -STOP program_name
    pkill -f -STOP program_name_or_args
  ```

  Also it could be send CONT signal to a program stopped with `Ctrl` + `z`.
