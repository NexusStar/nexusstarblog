---
layout: single
title: "Effort multiplied by tools"
excerpt: "tools I use for making things done"
modified: 2016-08-05
date: 2016-08-05
tags: [general, programming]
comments: true
author_profile: true
---

I like [ Seth's Blog ](http://sethgodin.typepad.com/). Once a day he put some witty two three liner
that make me think.

Today was about conservation of effort.

> Without a doubt, a good tool helps us do a task better, but often, particularly online, there's so much effort and overhead and fear associated with the tools we use that sometimes they don't leverage our work as much as we give them credit for.

This was stated about different set of tools not IDE, text editors and such but nevertheless rolls my head.

Not before long I was stickler to IDE simply adore complexity of MS VisualStudio gawk at every tool and update JetBrains do, install on the first day of release VS Code. Got a gist for Atom configuration and learn by hearth Sublime Text short cuts.

Simply put I was of search for the best tool of the trade read post and watch videos, tutorial and even bought books about one tool or another.

But somewhere on the way just switch to Vim and from there to Neovim this was somehow natural transition as I do more or the less mostly front end stuff which involves a lot of CLI tools configuration files and mostly scripting languages such as JavaScript web projects written mostly on PHP and so on.

In the last 10 weeks I learned C++ and was astonished to find that although the required IDE for the course was VisualStudio I can manage easily without it just with little configuration of Neovim.

### So currently I use:

* Preferred text editor: Neovim
* CSS preprocessor: SASS or Less
* Task manager: Gulp
* Vitalization: WM Ware workstation
* Preferred OS: Linux (Ubuntu or Arch)
* Preferred version control tool: Git
* Bootstrap and Foundation frameworks
* I know about and digg a lot into Docker.
* Just started with C++ and love it.
* Think that JavaScript is great and know it.
* It may sound strange but I think that C# is great also.

### What I plan to learn:

* React as UI tool
* TypeScript as subset of JS
* Dig deeper in ES6
* Qt framework for C++

So where that puts me I don't know maybe it's just curiosity and my hunger for learning new things.

