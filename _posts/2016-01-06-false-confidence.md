---
layout: single
title: "False confidence due to new found pride"
excerpt: "Why do so many beginner programmers think they should build everything from scratch?"
modified: 2016-07-26
tags: [personal, programming]
comments: true
author_profile: true
---
*Why do so many beginner programmers think they should build everything
from scratch?*

Just read answers to this question on [ Quora ](https:/quora.com).

Some of the answers are astoundingly on point not so on the question as general life knowledge.

_Beginner programmers are stricken with false confidence due to their newfound pride._

It feels good to be told you’re smart, or doing something that many could not. Because of this, and many other factors resulting in excessive hubris, beginners manage to think they know how things should be built. They know the best way to accomplish the task at hand. They *know* that they can build it better than anyone else can. But they *don’t know as much as they think they know*.

Although I think that building something from scratch is the only way to learn how inner parts work and fit together. Don't get me wrong if there is framework at hand everyone should start with it but make sure that it works and works really well.

When I start I tend to create everything from base up this takes so much time and was so hard to explain to clients employees etc. I know inside out bootstrap, foundation and some small frameworks but problem with every framework is that it brings huge back load on you and if you don't know it really well the whole thing could be huge PITA.
