---
layout: single
title: "Don't put random code just to make it work"
excerpt: "stuttering with code"
modified: 2016-08-05
date: 2016-08-05
tags: [intro, beginner, programming]
comments: true
author_profile: true
---

Some of time consuming mistakes I made when learning some new
language or technology is trying to make something to work by
changing code randomly.

This is actually _code stutter_ continuously repeating one and
the same mistakes in hope code "to work".

What's the mistake in that when something does not work there is
particular reason for that and in the primary confusion I just throw
changes with mindset set to "maybe this will work" "or this".

And what actually should I do:

## First and most important one _stop for a moment_
   - Just stop whatever doing right now and change the rhythm.
   - Distract for a few moments.

  > This is not life saving operation so nothing
  > life threatening will happen in the next 5 min.

## Determine what exactly was wrong.

  - There always is some error message and if not where are the clues?
  - Do you have the knowledge about this problem?
  - Where could you find the solution or problem explanation?

  * With current knowledge what the solution could be

  > As finding _the right_ solution could be very time consuming
  > proven by experience time and time again.
  > Just make it work with what you currently know and leave improvement
  > for later time next version etc.

## Document the problem and solution

  This is proven to be one of the most time savers when dealing with
  the similar problem or trying to do refactoring improvement etc.
