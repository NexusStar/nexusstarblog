---
layout: single
title: "Winning the lottery"
excerpt: "following Seth Godin post"
modified: 2016-08-16
date: 2016-08-16
tags: [general, study, programming]
comments: true
author_profile: true
---
## The Source

Seth's Blog post [ The lottery winners(a secret of unhappiness) ](http://sethgodin.typepad.com/seths_blog/2016/08/the-lottery-winners-a-secret-of-unhappiness.html) as always hit the bulls eye.

> You're going to have to fight for every single thing, forever and ever. It's really unlikely that they will pick you, anoint you or hand you the audience and support you seek.
> No one will ever realize just how extraordinary you are, how generous, charismatic, or caring.

## The crackdown

As hard is to be realized this is just the truth no one ever will research how good you are at something.

There is always someone better, more trained awarded you name it. But this does not mean that they will
get the job done better.
Place, project whatever is done by people on site those individual's that are at the right place
at the right moment. Those people are not best skilled or best trained just available there.
To place yourself in that place and moment is true art and skill and this skills does not correspond
with what you work or are good at.
The need to promote and advertise yourself in the age of information overload is more than important.
And something not so funny everyone need to know the right people and have healthy does of luck.

So have luck and try to win over the deep.



