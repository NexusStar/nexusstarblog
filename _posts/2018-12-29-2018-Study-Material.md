---
layout: single
title: "2018 Study Materials"
excerpt: "2018 study plan from JavaScript developer point of view"
modified: 2017-12-29
date: 2017-12-29
tags: [programming, JavaScript, begginer]
comments: true
author_profile: true
---

After looking at lal the trends ahead, here's one recomended learning plan for 2018 to remain competitive as a remote developer and 
unleash my potential:
 
* Learn [ Vue.js ](https://vuejs.org/v2/guide/)

* [ GraphQL ](http://graphql.org/learn/)  is a must for 2018
* Storybook! StoryBook! [ Storybook ](https://storybook.js.org/)!
* Install [ Prettier ](https://github.com/prettier/prettier)
 on a project and make code more readable.
* Learn and use [ Jest ](https://facebook.github.io/jest/) snapshots in tandem with [ Enzyme ](https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f) on React project.
* [ TypeScript ](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
* Consider [ Gatsby ](https://www.gatsbyjs.org/blog/2017-12-06-gatsby-plus-contentful-plus-netlify/)
 for building something from Markdown -> Static Pages
* Build a mobile app using [ React Native ](https://egghead.io/courses/react-native-fundamentals) or desctop one with [ Electron ](https://medium.freecodecamp.org/how-to-build-your-first-app-with-electron-41ebdb796930)
* Play around with [ Popmotion ](https://popmotion.io/learn/get-started/) to create smooth animation
