---
layout: single
title: "Spacemacs install on Windows"
excerpt: "all things to install in order to run spacemacs on Windows"
modified: 2016-12-27
date: 2016-12-27
tags: [general, config, Windows, emacs]
comments: true
author_profile: true
---
## Spacemacs
Recently I start using Spacemacs as Windows editor comforatable
substitute for Neovim in Windows OS. As always installation 
is not stight forward there are more than one source of information I
dig out and try few things before all works as I like it.

For now the experience is quite plesant interface is quite nice and 
the system shows a lot of promise here is short instruction of installation
and all things need to be configured.

Note that I still could not make Aspell to work

## Spacemacs basic installation

### Install Emacs for windows use [ftp.gnu.org mirrors](http://ftpmirror.gnu.org/emacs/windows)
 1. Download relevant zip file and unzip in some nice dir e.g `C:\emacs`
 1. Add emacs dir bin to `path` Windows variable `C:\emacs\bin`
 1. Crete `home` directory e.g `C:\home`
 1. Crete new Windows system variable _HOME_ and add to it `home` dir path
    * Go to _Control Panel -> System -> Advanced System Settings -> Environmen Variables_
    * Under `System variables` click on `New` and enter following: 
      - `Variable name: HOME`
      - `Variable value: C:\home`
1. Navigate to Emacs install dir `C:\emacs`
  - run `addpm.exe` it should show pop-up with message `Install Emacs at C:\emacs` 
  - navigate to `C:\home` and check that there is `.emacs.d` directory 
  - run `runemacs.exe` to check that all is working
  
### Install Spacemacs use [GitHub source](https://github.com/syl20bnr/spacemacs)
 
#### Prerequisites
 
_Make sure you've got Git and it's added to `path` Windows variables_
 
 1. Rename `.emacs.d` to `.emacs.d_bak` or remove altogether 
 1. Clone sourcefiles `git clone https://github.com/syl20bnr/spacemacs.git ~/.emacs.d` 
 1. Configure [Quelpa](https://github.com/quelpa/quelpa) I use Native Emacs install
 1. Start Emacs install and update `.spacemacs` with personal [configuration](https://gist.github.com/nexusstar/bcd0e3de3db03cdfb38c31d4be10f3e3)



 
