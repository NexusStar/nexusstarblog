---
layout: single
title: "JavaScript For loops"
excerpt: "family of For Loops"
modified: 2017-12-30
date: 2016-12-30
tags: [programming, JavaScript, begginer]
comments: true
author_profile: true
---
## Iteration in JavaScript

JavaScript have few ways to loop over any type of data that is [_iterable_](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols).
This allows to loop over Array, Map and Set, not Object (by default objects are not iterable).

### For loop
```javascript

const digits = [0,1,2,3,4,5,6,7];

for (let i = 0; i < digits.length; i++){
  console.log(digits[i]);
}

```
_pros:_ It's loop construct present in most programming languages

_cons:_ 
* It should keep track of _the counter_ and _exit condition_. 
* Not all data is structured like an array , so for loop isn't always an option.

### The for in loop

```javascript

const digits = [0,1,2,3,4,5,6,7];

for (const index in digits){
  console.log(digits[index]);
}

```
_pros_: improve upon for loop by elimination of the counting logic and exit condition

_cons_:

  * Still have to deal with index to access the values of the array
  * loops over all enumerable properties so if we add additional properties to the array's prototype, then those properties will also appear in the loop.

```javascript

Array.prototype.decimalfy = function(){
  for (let i = 0; i < this.length; i++){
    this[i] = this[i].toFixed(2);
  }
};

const digits = [0,1,2,3,4,5,6,7];

for (const index in digits){
  console.log(digits[index]);
}

```
prints

```
0
1
2
3
4
5
6
7
function() {
   for (let i = 0; i < this.length; i++) {
      this[i] = this[i].toFixed(2);
     }
}
```

### forEach loop

_forEach_ is another type of for loop in JavaScript. However [forEach()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach) is actually an array method.
so it can only be used exclusively with arrays. There is also no way to stop or break a forEach loop.
